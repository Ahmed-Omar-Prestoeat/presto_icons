// ignore_for_file: constant_identifier_names

library presto_icons;

import 'package:flutter/material.dart';

class PrestoIconsData extends IconData {
  const PrestoIconsData(int codePoint)
      : super(codePoint, fontFamily: 'PrestoIcons');
}

class PrestoIcons {
  static const IconData twitter_fill = PrestoIconsData(0xe807);
  static const IconData burger_fill = PrestoIconsData(0xe808);
  static const IconData star_fill = PrestoIconsData(0xe806);
  static const IconData cart_fill = PrestoIconsData(0xe803);
}
